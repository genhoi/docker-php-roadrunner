Docker image with PHP and [Roadrunner](https://github.com/spiral/roadrunner)

User `app` created after start container with `APP_USER_ID` environment variable, `APP_USER_ID=1001` by default.

Environment variables
 - ROADRUNNER_CONF `/etc/roadrunner/.rr.yaml` Roadrunner [config file](https://roadrunner.dev/docs/intro-config)
 - APP_DIR `/app` PHP application folder
 - APP_USER `app` Username created after start container
 - APP_USER_ID `1001` User id for mount app volume from host
 - PHP_INI_SCAN_DIR `:/var/lib/php` can be set to override the scan directory set via the configure script

Example docker-compose.yaml
```yaml
version: "3.3"
services:
  app:
    image: genhoi/php-roadrunner:7.3
    volumes:
      - './config/php:/var/lib/php'
      - './app:/app'
      - './config/roadrunner/.rr.yaml:/etc/roadrunner/.rr.yaml:delegated'
    environment:
      APP_USER_ID: 1001
```