FROM alpine:3.9.6

ARG WITH_DEBUG=0

ENV ROADRUNNER_VERSION 1.8.4
ENV ROADRUNNER_CONF /etc/roadrunner/.rr.yaml

ENV APP_DIR /app
ENV APP_USER app
ENV APP_USER_ID 1001
ENV APP_GROUP_ID 1001
ENV PHP_INI_SCAN_DIR :/var/lib/php

RUN apk add --update --no-cache \
        php7 \
        php7-amqp \
        php7-apcu \
        php7-bcmath \
        php7-calendar \
        php7-common \
        php7-ctype \
        php7-curl \
        php7-dom \
        php7-fileinfo \
        php7-ftp \
        php7-iconv \
        php7-intl \
        php7-json \
        php7-mbstring \
        php7-opcache \
        php7-openssl \
        php7-pdo \
        php7-pdo_pgsql \
        php7-pdo_mysql \
        php7-mysqli \
        php7-pgsql \
        php7-phar \
        php7-posix \
        php7-redis \
        php7-session \
        php7-pcntl \
        php7-simplexml \
        php7-sodium \
        php7-soap \
        php7-sockets \
        php7-tokenizer \
        php7-xml \
        php7-xmlwriter \
        php7-xmlreader \
        php7-zip \
        php7-zlib \
        curl git openssh-client && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

## Install and Enable xdebug
RUN if [ $WITH_DEBUG -eq "1" ]; then \
        apk add --update --no-cache php7-xdebug && \
        echo 'zend_extension=xdebug.so' > /etc/php7/conf.d/xdebug.ini; \
    fi

# Download RoadRunner
RUN mkdir -p /tmp/rr && \
    cd /tmp/rr && \
    wget https://github.com/spiral/roadrunner/releases/download/v${ROADRUNNER_VERSION}/roadrunner-${ROADRUNNER_VERSION}-linux-amd64.tar.gz && \
    tar -xzf roadrunner-${ROADRUNNER_VERSION}-linux-amd64.tar.gz && \
    mv roadrunner-${ROADRUNNER_VERSION}-linux-amd64/rr /usr/local/bin  && \
    rm -rf /tmp/rr

COPY entry.sh /entry.sh
RUN chmod u+x /entry.sh

ENTRYPOINT ["/entry.sh"]
CMD /usr/local/bin/rr serve -d -c ${ROADRUNNER_CONF} -w ${APP_DIR}